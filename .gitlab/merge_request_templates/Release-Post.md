**Review Apps**: https://release-X-Y.about.gitlab.com/YYYY/MM/22/gitlab-X-Y-released/

_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/YYYY_MM_22_gitlab_X_Y_released.yml
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Home page banner**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/includes/hello-bar.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/release-managers/

### General contributions

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

All contributions added by team members, collaborators, and Product Managers (PMs).

#### Author's checklist

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

The PM leading the post is responsible for adding and checking the following items:

- [ ] Label MR: ~"blog post" ~release ~"release post" ~P1
- [ ] Assign the MR to yourself
- [ ] Add milestone
- [ ] Update the links and due dates in this MR description
- [ ] Make sure the blog post initial files, as well as this MR template contain the latest templates
- [ ] Add authorship (author's data)
- [ ] Ask the Marketing reviewer to add the introduction
- [ ] Add MVP (feature block)
- [ ] Add MVP to `data/mvps.yml`
- [ ] Add [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image) (`image_title`) (scaled and compressed)
- [ ] Add [social sharing image](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image) (`twitter_image`) (scaled and compressed)
- [ ] Make sure all feature descriptions are positive and cheerful
- [ ] Make sure all features listed in the [direction](https://about.gitlab.com/direction/)
page are included in the post
- [ ] Mention the [release managers](https://about.gitlab.com/release-managers/) to remind them
to add the [upgrade barometer](https://about.gitlab.com/handbook/marketing/blog/release-posts#upgrade-barometer) section
- [ ] Mention the team leads to remind them to add the performance improvements: Yorick, Douwe, Sean, Remy, Stan, Kamil, Tim Z., Clement, Andre
- [ ] Ping @atflowers and @aoetama to add the Release Radar webcast link
- [ ] Make sure Upgrade barometer is in
- [ ] Check which one is the top feature (with Job and William)
- [ ] Check if deprecations are included
- [ ] Alert people one working day before each due date (post a comment to #release-post Slack channel)
- [ ] Remove remaining template sections
- [ ] Make sure all images (png, jpg, and gifs) are smaller than 300 KB each
- [ ] Run the release post through an automated spell and grammar check
- [ ] Perform the [content review](#content-review)

#### Recurring blocks

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

The following sections are always present, and managed by the PM or Eng lead
owning the related area:

- [ ] Add GitLab Runner improvements: Fabio
- [ ] Add Omnibus improvements: Joshua
- [ ] Add Mattermost update to the Omnibus improvements section: Victor
- [ ] Add Performance improvements: Douwe, Sean, Kamil, Yorick, Tim

#### Feature blocks

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

The Product Managers are responsible for adding their feature blocks to the
release post by the due date for general contributions. PMs are also responsible
for adding any notable Community Contributions.

PMs: please check your box only when **all** your features and deprecations were
added with completion (documentation links, images, etc). Pls don't check if
there are still things missing.

- [ ] Andreas (@akaemmerle)
- [ ] Daniel (@danielgruesso)
- [ ] Fabio (@bikebilly)
- [ ] James (@jramsay)
- [ ] Jason (@jlenny)
- [ ] Jeremy (@jeremy_)
- [ ] Joshua (@joshlambert)
- [ ] Victor (@victorwu)

Tip: make your own checklist:

- Primary features
- Improvements (secondary features)
- Deprecations
- Documentation updated
- Documentation links added to the post
- Community contributions
- Illustrations added to the post (compressed)
- Update `features.yml` (with accompanying screenshots)

### Review

Ideally, complete the review until the **4th** working day before the 22nd,
so that the 3rd and the 2nd working day before the release
could be left for fixes and small improvements.

#### Content review

**Due date: YYYY-MM-DD** (5th working day before the 22nd)

Performed by the author of the post:

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] Check all comments in the thread (make sure no contribution was left behind)
- [ ] Check Features' names
- [ ] Check Features' availability (Core, Starter, Premium, Ultimate badges)
- [ ] Check Documentation links (all feature blocks contain `documentation_link`)
- [ ] Make sure `documentation_link` links to feature webpages when available
- [ ] Update home page banner`source/includes/hello-bar.html.haml`
- [ ] Features were added to `data/features.yml` (with accompanying screenshots)
- [ ] Check all images size < 300KB (compress them all with [TinyPNG](https://tinypng.com/) or similar tool)
- [ ] Pull `master`, resolve any conflicts
- [ ] Make sure all discussions in the thread are resolved
- [ ] Mention Mark Pundsack to remind him to review
- [ ] Assign the MR to the next reviewer (structural check)
- [ ] Lock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) with File Locking **on the 21st**

#### Structural check

**Due date: YYYY-MM-DD** (3rd working day before the 22nd)

Performed by technical writers (Axil, Evan, Marcia, or Mike):

- 1. Structural check
- [ ] Add the label ~review-structure
- [ ] Check frontmatter (entries, syntax)
- [ ] Check `image_title` and `twitter_image`
- [ ] Check image shadow applied correctly: `{image_noshadow: true}` when an image already has shadow
- [ ] Videos/iframes wrapped in `<figure>` tags (responsiveness)
- [ ] Wrap text
- [ ] Check headers in sentence case
- [ ] Check feature and product names in capital case
- [ ] Check if images are harmonic/consistent
- [ ] Add/check cover img reference (at the end of the post)
- [ ] Columns (content balance between the columns)
- [ ] Meaningful links (SEO)
- [ ] Badges consistency (applied to all headings)
- [ ] Double check documentation updates
- [ ] Check documentation links (point to `/ee/`, not to `/ce/`)
- [ ] Check about.gitlab.com links: relative URLs
- [ ] Check the anchor links in the intro
- [ ] Remove any remaining instructions
- [ ] Remove HTML comments
- [ ] Run [deadlink checker](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf)
- [ ] Pull `master`
- [ ] Update release template with any changes (if necessary)
- [ ] Post a comment in the `#general` channel linking to the review app + MR reminding the team to take a look at the RP and report any problems in the `#release-post` channel. (Do NOT ping `@all` or `@channel`)
- [ ] Remove the label ~review-structure
- [ ] Assign the MR to the next reviewer (copyedit)

#### Further reviews

**Due date: YYYY-MM-DD** (2nd working day before the 22nd)

- [ ] Copyedit (content team)
  - [ ] Title
  - [ ] Description
  - [ ] Intro
  - [ ] Grammar, spelling, clearness (body)
  - [ ] [Homepage Blurb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/includes/home/ten-oh-announcement.html.haml)
  - [ ] Tweet social sharing text (for Twitter, FB, and LinkedIn)
  - [ ] Assign the MR to the next reviewer (marketing)
- [ ] Marketing review (PMMs - [messaging lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead))
  - [ ] Write the introduction (**by the 6th day before the 22nd**)
  - [ ] Check/copyedit feature blocks
  - [ ] Check/copyedit `features.yml`
  - [ ] Check/copyedit homepage blurb
  - [ ] Check/copyedit social sharing text
  - [ ] Mention Job for final check
  - [ ] Remove the label ~review-in-progress
  - [ ] Assign the MR back to the author

### Merge it :rocket:

The author of the post is responsible for merging the MR and following up
with possible adjustments/fixes.

#### Last check before merging

- [ ] Read the [important notes](#important-notes) below
- [ ] Mention `@matteeyah` to remind him to send the swag pack to the MVP
- [ ] Check if all the anchor links in the intro are working
- [ ] Check if there are no broken links
- [ ] Pull `master` and fix any conflicts
- [ ] Check if there isn't any alert on Slack's `#release-post` and `#general` channels
- [ ] Check if there isn't any alert on this MR thread
- [ ] Check if the tweet copy is ready and someone is ready to share on social media
- [ ] Ask the release managers to ping you when the packs are publicly available (and GitLab.com is up and running on the release version)
- [ ] Unlock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) just before merging
- [ ] Merge the MR
- [ ] Wait for the pipeline
- [ ] Check the look on social media with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/)
- [ ] Share on social media

#### Important notes

- The post is to be merged on the **22nd** at 15:00 UTC and as soon as
GitLab.com is up and running on the new release
version, and all packages are publicly available. Not before.
- The usual release time is **15:00 UTC** but it varies according to
the deployment. If something comes up and delays the release, the release post
will be delayed with the release.
- Coordinate the timing with the
[release managers](https://about.gitlab.com/release-managers/).
Ask them to keep you in the loop.
- After merging, wait a few minutes to see if no one spot an error (usually posted in #general),
then share on Twitter, Facebook, and LinkedIn, or make sure someone (Emily vH, JJ, Marcia) does.
- Keep an eye on Slack and in the blog post comments for a few hours to make sure no one found anything that should be fixed
