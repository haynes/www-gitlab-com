---
layout: job_page
title: "Manager, Sales and Business Development"
---

You have experience in sales and business development helping people get started with a technical product. Your job is four-fold: (1) Help generate and increase demand for GitLab, (2) connect people interested in GitLab to the sales team or other appropriate resources, (3) lead and train other members of the business development, sales development, and smb customer advocate teams, and (4) take on operational and administrative tasks to help the sales and business development team perform.

## Responsibilities

* Meet or exceed Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Lead the Sales Development team by:
    * Partnering with Regional Sales Directors (RDs) to identify key company accounts for the Sales Development Representatives (SDRs) to focus on.
    * Ensuring key accounts benefit from coordinated activity from SDRs, Pipe-to-Spend, and Field Marketing.
    * Training other members of the Sales Development team to navigate key accounts to uncover potential sales opportunity.
* Meet or exceed volume targets for Business Development Representative (BDR) qualified leads that convert to Sales Accepted Opportunities (SAO).
* Lead the Business Development team by:
    * Identifying where leads are in their buying process and taking appropriate action to help them on their way to becoming a GitLab customer.
    * Ensuring BDR qualified leads are assigned to the sales team and are followed up with effectively.
    * Training other members of the business development team to identify, qualify, and assign inbound leads.
* Meet or exceed incremental annual contract value (IACV) targets in the small and medium business (SMB) sales segment.
* Lead the SMB Customer Advocate team by:
    * Coaching the team on how to better assist SMB customers through product demonstrations and assisting with ensuring they have a positive experience when they trial and purchase GitLab.
    * Coaching the team on effective account management to ensure customers stay customers and expand their use of GitLab.
    * Working with the online growth and marketing operations teams to eliminate any friction in the SMB purchase process and improve efficiency.
* Lead hiring and onboarding of new Sales Development Representatives, Business Development Representatives, and SMB Customer Advocates.
* Ensuring all team members improve performance and abilities over time by providing coaching and feedback during weekly 1:1s and team calls.
* Work with Pipe-to-Spend on coordinating inbound and outbound marketing tactics with SDR and BDR activities.
* Work with Content Marketing on refining the message and content offers we take to market.
* Work with Field Marketing on event lead qualification, event registration/attendance, and follow up.
* Work with Product Marketing on activating buyer personas, refining product demos, and training the teams on our messaging & positioning.
* Work with the Sales team to ensure relationships with key accounts are developed and strengthened.
* Identify and define key sales metrics, measure lead-to-opportunity process, and create goals that drive growth.
* Document all processes in the handbook and update as needed.
* Providing input to Chief Marketing Officer and Senior Director of Marketing & Sales Development regarding team practices.

## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service for a technical product - leadership experience is highly preferred.
* Experience with CRM software (Salesforce preferred)
* Experience in sales operations and/or marketing automation software preferred
* Understanding of B2B software, Open Source software, and the developer product space is preferred
* Passionate about technology and learning more about GitLab
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.
