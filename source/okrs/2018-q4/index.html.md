---
layout: markdown_page
title: "2018 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: TBD IACV Goal

* VPE
  * Support:
    * EMEA:
    * APAC:
	* AMER East:
	* AMER West:

### CEO: TBD Product Goal

* VPE:
  * Frontend:
  * Dev Backend:
    * Plan:
      * Preserve 100% of [error budget]: 100% ()
      * Complete phase 1 of preparedness for [Elasticsearch on GitLab.com]
    * Create:
      * Preserve 100% of [error budget]: 100% ()
    * Manage:
      * Preserve 100% of [error budget]: 100% ()
    * Geo:
      * Preserve 100% of [error budget]: 100% ()
      * Complete all issues in Phase 1 of the DR/HA plan working with the Production Team ()
  * Ops Backend:
      * Configure: Preserve 100% of [error budget]: 100% ()
      * Configure: Merge smaller changes more frequently: &gt; 10 average MRs merged per week, &lt; 100 average lines added, &lt; average 7 files changed
      * [Philippe Lafoucrière](https://about.gitlab.com/team/#plafoucriere): Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops-backend/secure/#security_products)
      * [Philippe Lafoucrière](https://about.gitlab.com/team/#plafoucriere): One CFP (Call For Proposals) accepted for a Tech conference
  * Infrastructure:
  * Quality:
    * Complete [Review Apps for CE and EE with GitLab QA running]: 100% ()
    * Complete phase 1 & 2 of [Addressing database discrepancy for our on-prem customers] : 100% ()
  * UX:
  * Security:

### CEO: TBD Team Goal

* VPE
  * Frontend:
  * Dev Backend:
    * Plan:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
    * Create:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
      * Get 10 MRs merged into Go projects by team members without pre-existing
        Go experience: X merged (X%)
    * Manage:
      * Source 25 candidates (at least 5 by direct manager contact) by
        October 15 and hire 2 developers: X sourced (X%), X hired (X%)
      * Team to deliver 10 topic-specific knowledge sharing sessions
        (“201s”) by the end of Q4 (X completed)
      * Geo:
        * At least one Geo team member to advance to Geo Maintainer ()
    * Stan:
      * [Reduce baseline memory usage in Rails by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49702)
      * [Reduce runtime memory usage in Sidekiq for top 5 workers by 30%](https://gitlab.com/gitlab-org/gitlab-ce/issues/49703)
      * Resolve 3 P1 Tier 1 customer issues
  * Infrastructure:
  * Ops Backend:
    * Configure: Source 60 candidates by Nov 15 and hire 2 developers: 0 sourced (0%), hired 0 (0%)
  * Quality:
    * Source 100 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 test automation engineers: X sourced (X%), X hired (X%)
  * Security:
  * Support:
  * UX:

[error-budget]: /handbook/engineering/#error-budgets
[Elasticsearch on GitLab.com]: https://gitlab.com/groups/gitlab-org/-/epics/153
[Review Apps for CE and EE with GitLab QA running]: https://gitlab.com/groups/gitlab-org/-/epics/265
[Cross-browser and mobile browser test coverage]: https://gitlab.com/gitlab-org/quality/team-tasks/issues/45
[Addressing database discrepancy for our on-prem customers]: https://gitlab.com/gitlab-org/gitlab-ce/issues/51438
